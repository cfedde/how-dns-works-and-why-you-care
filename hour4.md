# Hour 4 

https://imgs.xkcd.com/comics/tech_support_cheat_sheet.png

# Looking Stuff Up

* host, dig
* nslookup
* tcpdump tshark
* The Standards Process

# Demo

## I want to look up `small.fedde.us`. What does my resolver do?

```
dig . ns @8.8.8.8
dig us ns @f.root-servers.net.
dig fedde.us ns @e.cctld.us.
dig small.fedde.us a @ns33.domaincontrol.com.
```

## An easy way to watch this run.

```
dig +trace small.fedde.us @8.8.8.8
```

## How is email handled by fedde.us?
```
dig mx fedde.us
```

## How do I find my sip server

```
dig _sip._udp.sip.voice.google.com SRV
```

## Why is there no SRV record for http?

https://imgs.xkcd.com/comics/standards.png

One thought is divergent standards

## Host is simpler interface than dig but not as informative

```
host ftp.uu.net
host www.google.com
```

```
dig www.verizon.com
host www.verizon.com
```

## NSLookup is old crufty deprecated and everyone refuses to give it up.

```
nslookup 0.pool.ntp.org
```

## tcpdump and tshark

Terminal based network sniffers.

```
sudo tcpdump -vvv -ttt -A port 53
```

```
sudo tshark -T json port 53
```

# The Standards Process

As standards bodies go the IETF is pretty cool.

* Open participation. Technical requiments.  
* There must be an implementation before a draft can be accepted as a standard.

# Pair and Share

* Try all this stuff  
* See if any of it makes sense 
* 
[Hour 4 notes](hour4_notes.md)
